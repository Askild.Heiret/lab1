package rockPaperScissors;

import java.util.*;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        while(true) {
            System.out.println("Let's play round " + roundCounter);
            String humanChoice = userChoice();
            String computerChoice = randomChoice();
            if (isWinner(humanChoice, computerChoice)) {
                humanScore += 1;
                System.out.println("Human chose " + humanChoice + ", computer chose " + computerChoice + " Human wins");
            } else if (isWinner(computerChoice, humanChoice)) {
                computerScore += 1;
                System.out.println("Human chose " + humanChoice + ", computer chose " + computerChoice + " Computer wins");
            } else {
                System.out.println("Human chose " + humanChoice + ", computer chose " + computerChoice + "It's a tie");
            }

            System.out.println("Score: human " + humanChoice + ", computer " + computerScore);

            roundCounter += 1;

            String continueAnswer = continuePlaying();
            if(continueAnswer.equals("n")){
                break;
            }
        }
        System.out.println("Bye bye :)");
    }

    private Boolean validateInput(String input, ArrayList<String> validInput){
        input.toLowerCase();
        return validInput.contains(validInput);
    }


    private boolean isWinner(String choice1, String choice2){
        if(choice1.equals("paper")){
            return choice2.equals("rock");
        }
        if(choice1.equals("scissors")){
            return choice2.equals("paper");
        }
        else{
            return choice2.equals("scissors");
        }
    }


    private String userChoice(){
        String userHand = readInput("Your choice (Rock/Paper/Scissors)?");
        return userHand;
    }

    private String randomChoice(){
        Random r = new Random();
        int choice = r.nextInt(3);
        return rpsChoices.get(choice);
    }


    private String continuePlaying(){
        while(true) {
            String continueAnswer = readInput("Do you wish to continue playing? (y/n)?");
            if (continueAnswer.equals("y") || continueAnswer.equals("n")) {
                return continueAnswer;
            } else {
                System.out.println("I don't understand " + continueAnswer + ". Try again");
            }
        }
    }


    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
